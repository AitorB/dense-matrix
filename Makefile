FCFLAGS = -O2 -fopenmp
FC = gfortran

ALL = densematrix

all: $(ALL)

dense-matrix: densematrix.o
	$(FC) $(FCFLAGS) $^ -o $@
	export OMP_NUM_THREADS=1; ./$@
	export OMP_NUM_THREADS=2; ./$@
	export OMP_NUM_THREADS=4; ./$@
	export OMP_NUM_THREADS=8; ./$@
	export OMP_NUM_THREADS=16; ./$@
	export OMP_NUM_THREADS=24; ./$@

%.o:%.f90
	$(FC) $(FCFLAGS) -c $^

.PHONY: clean
clean:
	rm -f *.o *~
	rm -f $(ALL)
