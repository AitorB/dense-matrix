program densematrix
    !$ use omp_lib 
    implicit none
    save

    integer :: i, j, n
    real :: x
    real(8) :: t0, t1
    real, dimension(10000,10000) :: A
    real, dimension(10000) :: vectorx
    real, dimension(10000) :: vectory

    n = size(vectory)

    do i=1,n
        do j=1,n
            call RANDOM_NUMBER(x)
            A(i,j) = x
        end do
        call RANDOM_NUMBER(x)
        vectorx(i) = x                                                           
    end do
    
    t0 = 0.0d0
    
    !$ t0 = omp_get_wtime();
    !$omp parallel default(none) private(j,i) shared(A,vectorx,vectory,n) 
    !$omp do schedule(static,1250) reduction(+:vectory)
    
    do i=1,n
        do j=1,n
            vectory(i) = vectory(i) + A(i,j)*vectorx(j)
        end do
    end do
    
    !$omp end do
    !$omp end parallel 
    
    t1 = 0.0d0
    !$ t1 = omp_get_wtime();
    
    write(*,*) " Time: ", t1 - t0, "s"

end program

